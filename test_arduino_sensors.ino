#include <Servo.h>
float mid;
int prev_angle = 0;
int manual = 0;
Servo servo;

void writeServo(int value)
{
  int angle = map(value, 800, 1020, 0, 180);
  servo.write(angle);  
  prev_angle = angle;
}

void writeLED(int value)
{
  int rotation = analogRead(A1);
  rotation = map(rotation,600,1023,900,1020);
  int brightness = map(value, rotation, 1020, 0, 255);
  
  analogWrite(2, brightness);
  Serial.print(brightness);
  Serial.print(" ");
}

void setup() 
{
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  mid = analogRead(A0);
  prev_angle = map((int)mid, 800, 1020, 0, 180);
  servo.attach(13);
  servo.write(0);    
}

void loop() 
{
  int temp = analogRead(A0);
  mid += (temp - mid)*0.1;
  writeLED(mid);
  writeServo(mid);
  
  Serial.println(mid);
  
  delay(20);
}
